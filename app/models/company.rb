class Company < ApplicationRecord
  include Filterable
  scope :search, -> (search) { where 'name LIKE ?', "%#{search.capitalize}%" }
end
