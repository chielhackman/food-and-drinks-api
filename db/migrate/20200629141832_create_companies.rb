class CreateCompanies < ActiveRecord::Migration[6.0]
  def change
    create_table :companies do |t|
      t.string :type
      t.string :name
      t.string :description
      t.string :address
      t.string :zip
      t.string :city

      t.timestamps
    end
  end
end
